import React from "react";

const Index = () => {
  const htmlMarkup = (html) => {
    return { __html: html };
  };

  return (
    <>
      <div className="wrapper-home-page">
        <h3>Home page</h3>
      </div>

      <div dangerouslySetInnerHTML={{ __html: `<h2>balabla</h2>` }} />

      <div dangerouslySetInnerHTML={htmlMarkup(`<h2>balabla</h2>`)} />

      <script
        dangerouslySetInnerHTML={{
          __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){
                window.dataLayer.push(arguments);
              }
              gtag('js', new Date());

              gtag('config', 'GTM-PB9V23W');
              `,
        }}
      />
    </>
  );
};

Index.getInitialProps = async (ctx) => {
  // ctx.store.dispatch(getHomeBanner(5));
  // ctx.store.dispatch(getHomePropose(3));
  // ctx.store.dispatch(getHomeProduct(8));
  // ctx.store.dispatch(getHomeArticle(3));
};

export default Index;
